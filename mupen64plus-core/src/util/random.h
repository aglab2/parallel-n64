#ifndef M64P_UTIL_RANDOM_H
#define M64P_UTIL_RANDOM_H

#include <stdint.h>

uint32_t random_u32();

#endif
