#!/bin/bash

SCRIPT_DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
SOURCE_DIR="$(realpath "${SCRIPT_DIR}/..")"

IMAGE_NAME="pn64-osx-builder"

USER_ID=$(id -u)
GROUP_ID=$(id -g)

docker build -t $IMAGE_NAME -f "$SCRIPT_DIR"/Dockerfile-mac "$SCRIPT_DIR"
docker run --rm -v "$SOURCE_DIR":/workspace -w /workspace -u $USER_ID:$GROUP_ID $IMAGE_NAME ./scripts/build-mac-universal.sh

