# ParallelN64 (Parallel Launcher Edition)

A fork of [ParallelN64](https://git.libretro.com/libretro/parallel-n64) that adds the GLideN64 graphics plugin and some additional settings and features. Designed to be used with Parallel Launcher.

# Credits

The original ParallelN64 can be found here: https://git.libretro.com/libretro/parallel-n64  

Contributors for the modifications specific to this fork:
 * [Matt Pharoah](https://gitlab.com/mpharoah)
   * Black border fix for ParaLLEl
   * Config to enable/disable unaligned DMA support
   * 16:10 (SteamDeck) widescreen support for GLideN64
   * Support for IS Viewer logging
   * Support for changing the time on the RTC
   * Support for storing the RTC state in savestates
 * [Wiseguy](https://gitlab.com/Mr-Wiseguy)
    * Raw gamecube controller support
 * [Aglab2](https://gitlab.com/aglab2)
    * MoltenVK (MacOS) support for ParaLLEl
    * Black border fix for NTSC roms in GLideN64
    * GLideN64 modifications for supporting old SM64 romhacks
    * Support for detecting save types based on EverDrive headers
    * Support for ROMs larger than 64 MiB
    * Native ARM support for MacOS
    * Support for f3dex3 in HLE GLideN64
 * [devwizard](https://gitlab.com/devwizard64)
    * Added support for SummerCart64 SD card emulation

### Platform Builds

ParallelN64 can be built for all supported platforms using Docker. On a Linux system, simply run any of the docker build scripts to build for that platform. The docker image will automatically be built the first time you run the script, and will be reused on every subsequent build. When building for multiple platforms, remember to run `./scripts/clean.sh` between builds on different platforms.
 * **Linux** `./scripts/build-nix64-docker.sh`
 * **Windows (64-bit)** `./scripts/build-win64-docker.sh`
 * **Windows (32-bit)** `./scripts/build-win32-docker.sh`
 * **MacOS (64-bit x86 only)** `./scripts/build-mac-docker-intel.sh`
 * **MacOS (Universal)** `./scripts/build-mac-docker-universal.sh`
